import requests # Import Package

class communicate(object):
	def __init__(self,git_api):
		self.params={
				'home_path':'https://api.github.com/',
				'Authentication':git_api,
				'user_repo_path':'user/repos'
		}

	def get_repo(self):
		url=self.params['home_path']+self.params['user_repo_path']
		headers={'Authorization':'token {}'.format(self.params['Authentication'])}
		resp=requests.get(url,headers=headers)
		return resp.json()

if __name__=='__main__':
	git_fetch=communicate('cefa92817b045b832de5e8b102f35075fc163b4e')
	#print(git_fetch.params)
	print(git_fetch.get_repo())