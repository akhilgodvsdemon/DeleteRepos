import json
import os
from Network.git_request import communicate
git_repo_path='./git_repos/'
git_repo_name='git_repos.json'
configuration_file='./config.json'
git_key='Git_Key'


if __name__=='__main__':
	with open(configuration_file) as file1:
		git_key_file=json.load(file1)

	if not os.path.isdir(git_repo_path):
		print('Getting all the Repos of the User in ./git_repos/repos.json')
		print(git_key)
		git_net=communicate(git_key_file[git_key])
		git_resp_json=git_net.get_repo()
		os.mkdir(git_repo_path)
		with open(git_repo_path+git_repo_name,'w') as file1:
			json.dump(git_resp_json,file1)
		print('Dumping Finished.....')
	else:
		print('Dump is aleady Present')